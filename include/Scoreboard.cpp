#include "../include/Scoreboard.h"

Scoreboard::Scoreboard()
{
	for (int i = 0; i < 2; i++) {

		for (int j = 0; j < 13; j++) {

			Score[j][i] = 0;

		}

	}

}


Scoreboard::~Scoreboard()
{
}


bool Scoreboard::displayChoices(int rolls[]) {
	std::string Choice = "";
	std::cout << "\tYou rolled : ";
	for (int i = 0; i < 5; i++) {
		std::cout << rolls[i] << " ";
	}
	std::cout << "\n";
	std::cout << "\tYour current score is: " + std::to_string(this->sumScores()) + "\n";
	std::cout << "\n\tPlease select from one of";
	std::cout << "\n\tthe following possible score combinations: ";
	std::cout << "\n\n\t*** Upper Section ***";
	std::cout << "\n\n\t1)'Aces' - Add all ones in hand.";
	std::cout << "\n\t2)'Twos' - Add all twos in hand.";
	std::cout << "\n\t3)'Threes' - Add all threes in hand.";
	std::cout << "\n\t4)'Fours' - Add all fours in hand.";
	std::cout << "\n\t5)'Fives' - Add all fives in hand.";
	std::cout << "\n\t6)'Sixes' - Add all sixes in hand.";
	std::cout << "\n\n\t*** Lower Section ***";
	std::cout << "\n\n\t7)'3 of a Kind' - Add all 5 dice.";
	std::cout << "\n\t8)'4 of a Kind' - Add all 5 dice.";
	std::cout << "\n\t9)'Full House' - 3 of a kind, and a separate pair. Worth 25 points.";
	std::cout << "\n\t10)'Small Straight' - Roll 4 sequential numbers. Worth 30 points.";
	std::cout << "\n\t11)'Large Straight' - Roll 5 sequential numbers. Worth 40 points.";
	std::cout << "\n\t12)'YAHTZEE!' - Roll 5 of the same number. Worth 50 points.";
	std::cout << "\n\t13)'Chance' - Add the total of all five dice.";
	std::cout << "\n\tdisplay) Type \"display\" to display already used scores.";
	std::cout << "\n\tend) Type \"end\" to end the game early\n\n\t";
	//this->grabChoice(rolls, Choice);

	return true;
}

void Scoreboard::grabChoice(int rolls[], std::string Choice) {

	if (Choice == "") {
		std::cin >> Choice;
	}

	else if (Choice == "1") {
		this->Aces(rolls);
	}
	else if (Choice == "2") {
		this->Twos(rolls);
	}
	else if (Choice == "3") {
		this->Threes(rolls);
	}
	else if (Choice == "4") {
		this->Fours(rolls);
	}
	else if (Choice == "5") {
		this->Fives(rolls);
	}
	else if (Choice == "6") {
		this->Sixes(rolls);
	}
	else if (Choice == "7") {
		this->ThreeOfAKind(rolls);
	}
	else if (Choice == "8") {
		this->FourOfAKind(rolls);
	}
	else if (Choice == "9") {
		this->FullHouse(rolls);
	}
	else if (Choice == "10") {
		this->SmallStraight(rolls);
	}
	else if (Choice == "11") {
		this->LargeStraight(rolls);
	}
	else if (Choice == "12") {
		this->Yahtzee(rolls);
	}
	else if (Choice == "13") {
		this->Chance(rolls);
	}
	else if (Choice == "display")
	{
		this->displayUsed(rolls);
		this->grabChoice(rolls, "");
	}
	else if (Choice == "end") {
		exit(0);
	}
	else {
		std::cout << "\tInvalid choice selected, please choose a valid category or type end\n\t";
		this->grabChoice(rolls,"");
	}


}
void Scoreboard::displayUsed(int rolls[]) {

	for (int j = 0; j < 13; j++) {

		if (Score[j][0] == 1)
		{
			if (j == 0)
			{
				std::cout << "\tAces: " << "Used" << std::endl;
			}
			else if (j == 1)
			{
				std::cout << "\tTwos: " << "Used" << std::endl;
			}
			else if (j == 2)
			{
				std::cout << "\tThrees: " << "Used" << std::endl;
			}
			else if (j == 3)
			{
				std::cout << "\tFours: " << "Used" << std::endl;
			}
			else if (j == 4)
			{
				std::cout << "\tFives: " << "Used" << std::endl;
			}
			else if (j == 5)
			{
				std::cout << "\tSixes: " << "Used" << std::endl;
			}
			else if (j == 6)
			{
				std::cout << "\t3 of a Kind: " << "Used" << std::endl;
			}
			else if (j == 7)
			{
				std::cout << "\t4 of a Kind: " << "Used" << std::endl;
			}
			else if (j == 8)
			{
				std::cout << "\tFull House: " << "Used" << std::endl;
			}
			else if (j == 9)
			{
				std::cout << "\tSmall Straight: " << "Used" << std::endl;
			}
			else if (j == 10)
			{
				std::cout << "\tLarge Straight: " << "Used" << std::endl;
			}
			else if (j == 11)
			{
				std::cout << "\tYAHTZEE: " << "Used" << std::endl;
			}
			else if (j == 12)
			{
				std::cout << "\tChance: " << "Used" << std::endl;
			}
		}
	}
	std::cout << "\t";

}
void Scoreboard::Aces(int rolls[]) {

	if (Score[0][0] == false) {
		for (int i = 0; i < 5; i++) {
			if (rolls[i] == 1)
			{
				Score[0][1] += rolls[i];
			}
		}
		Score[0][0] = 1;
		std::cout << "\n";
		std::cout << "\n\n\tTotal value of *One* dice you have: \n";
		std::cout << "\t" << Score[0][1] << std::endl;

	}
	else
	{
		std::cout << "\n\n\tYou've already filled this category! Please choose another\n";
		this->displayChoices(rolls);
	}


}
void Scoreboard::Twos(int rolls[]) {


	if (Score[1][0] == false) {
		for (int i = 0; i < 5; i++) {
			if (rolls[i] == 2)
			{
				Score[1][1] += rolls[i];
			}
		}
		Score[1][0] = 1;
		std::cout << "\n";
		std::cout << "\n\n\tTotal value of *Two* dice you have: \n";
		std::cout << "\t" << Score[1][1] << std::endl;

	}
	else
	{
		std::cout << "\n\n\tYou've already filled this category! Please choose another\n";
		this->displayChoices(rolls);
	}


}
void Scoreboard::Threes(int rolls[]) {
	if (Score[2][0] == false) {
		for (int i = 0; i < 5; i++) {
			if (rolls[i] == 3)
			{
				Score[2][1] += rolls[i];
			}
		}
		Score[2][0] = 1;
		std::cout << "\n";
		std::cout << "\n\n\tTotal value of *Three* dice you have: \n";
		std::cout << "\t" << Score[2][1] << std::endl;

	}
	else
	{
		std::cout << "\n\n\tYou've already filled this category! Please choose another\n";
		this->displayChoices(rolls);
	}



}
void Scoreboard::Fours(int rolls[]) {


	if (Score[3][0] == false) {
		for (int i = 0; i < 5; i++) {
			if (rolls[i] == 4)
			{
				Score[3][1] += rolls[i];
			}
		}
		Score[3][0] = 1;
		std::cout << "\n";
		std::cout << "\n\n\tTotal value of *Four* dice you have: \n";
		std::cout << "\t" << Score[3][1] << std::endl;

	}
	else
	{
		std::cout << "\n\n\tYou've already filled this category! Please choose another\n";
		this->displayChoices(rolls);
	}



}
void Scoreboard::Fives(int rolls[]) {


	if (Score[4][0] == false) {
		for (int i = 0; i < 5; i++) {
			if (rolls[i] == 5)
			{
				Score[4][1] += rolls[i];
			}
		}
		Score[4][0] = 1;
		std::cout << "\n";
		std::cout << "\n\n\tTotal value of *Five* dice you have: \n";
		std::cout << "\t" << Score[4][1] << std::endl;

	}
	else
	{
		std::cout << "\n\n\tYou've already filled this category! Please choose another\n";
		this->displayChoices(rolls);
	}



}
void Scoreboard::Sixes(int rolls[]) {


	if (Score[5][0] == false) {
		for (int i = 0; i < 5; i++) {
			if (rolls[i] == 6)
			{
				Score[5][1] += rolls[i];
			}
		}
		Score[5][0] = 1;
		std::cout << "\n";
		std::cout << "\n\n\tTotal value of *Six* dice you have: \n";
		std::cout << "\t" << Score[5][1] << std::endl;

	}
	else
	{
		std::cout << "\n\n\tYou've already filled this category! Please choose another\n";
		this->displayChoices(rolls);
	}



}
void Scoreboard::ThreeOfAKind(int rolls[]) {

	bool ThreeOfAKind = false;
	if (Score[6][0] == false) {
		for (int i = 1; i <= 6; i++) {
			int Count = 0;
			for (int j = 0; j < 5; j++)
			{
				if (rolls[j] == i)
					Count++;

				if (Count > 2)
					ThreeOfAKind = true;
			}
		}

		if (ThreeOfAKind)
		{
			for (int k = 0; k < 5; k++)
			{
				Score[6][1] += rolls[k];
			}
		}
		Score[6][0] = 1;
		std::cout << "\n";
		std::cout << "\n\n\tTotal value of your *Three Of A Kind*: \n";
		std::cout << "\t" << Score[6][1] << std::endl;

	}
	else
	{
		std::cout << "\n\n\tYou've already filled this category! Please choose another\n";
		this->displayChoices(rolls);
	}


}
void Scoreboard::FourOfAKind(int rolls[]) {
	bool FourOfAKind = false;
	if (Score[7][0] == false) {
		for (int i = 1; i <= 6; i++) {
			int Count = 0;
			for (int j = 0; j < 5; j++)
			{
				if (rolls[j] == i)
					Count++;

				if (Count > 3)
					FourOfAKind = true;
			}
		}

		if (FourOfAKind)
		{
			for (int k = 0; k < 5; k++)
			{
				Score[7][1] += rolls[k];
			}
		}
		Score[7][0] = 1;
		std::cout << "\n";
		std::cout << "\n\n\tTotal value of your *Four Of A Kind*: \n";
		std::cout << "\t" << Score[7][1] << std::endl;

	}
	else
	{
		std::cout << "\n\n\tYou've already filled this category! Please choose another\n";
		this->displayChoices(rolls);
	}


}
void Scoreboard::Yahtzee(int rolls[]) {
	bool Yahtzee = false;
	if (Score[11][0] == false) {
		for (int i = 1; i <= 6 && !Yahtzee; i++) {
			int Count = 0;
			for (int j = 0; j < 5; j++)
			{
				if (rolls[j] == i)
					Count++;

				if (Count > 4)
					Yahtzee = true;
			}
		}

		if (Yahtzee)
		{
			Score[11][1] = 50;
		}
		Score[11][0] = 1;
		std::cout << "\n";
		std::cout << "\n\n\tTotal value of your *YAHTZEE*: \n";
		std::cout << "\t" << Score[11][1] << std::endl;

	}
	else
	{
		std::cout << "\n\n\tYou've already filled this category! Please choose another\n";
		this->displayChoices(rolls);
	}
}
void Scoreboard::LargeStraight(int rolls[]) {

	if (Score[10][0] == false) {

		if (((rolls[0] == 1) &&
			(rolls[1] == 2) &&
			(rolls[2] == 3) &&
			(rolls[3] == 4) &&
			(rolls[4] == 5)) ||
			((rolls[0] == 2) &&
			(rolls[1] == 3) &&
				(rolls[2] == 4) &&
				(rolls[3] == 5) &&
				(rolls[4] == 6)))
		{
			Score[10][1] = 40;
		}
		Score[10][0] = 1;
		std::cout << "\n";
		std::cout << "\n\n\tTotal value of your *Large Straight*: \n";
		std::cout << "\t" << Score[10][1] << std::endl;

	}
	else
	{
		std::cout << "\n\n\tYou've already filled this category! Please choose another\n";
		this->displayChoices(rolls);
	}


}
void Scoreboard::SmallStraight(int rolls[]) {

	if (Score[9][0] == false)
	{
		for (int j = 0; j < 4; j++)
		{
			int temp = 0;
			if (rolls[j] == rolls[j + 1])
			{
				temp = rolls[j];

				for (int k = j; k < 4; k++)
				{
					rolls[k] = rolls[k + 1];
				}

				rolls[4] = temp;
			}
		}

		if (((rolls[0] == 1) && (rolls[1] == 2) && (rolls[2] == 3) && (rolls[3] == 4)) ||
			((rolls[0] == 2) && (rolls[1] == 3) && (rolls[2] == 4) && (rolls[3] == 5)) ||
			((rolls[0] == 3) && (rolls[1] == 4) && (rolls[2] == 5) && (rolls[3] == 6)) ||
			((rolls[1] == 1) && (rolls[2] == 2) && (rolls[3] == 3) && (rolls[4] == 4)) ||
			((rolls[1] == 2) && (rolls[2] == 3) && (rolls[3] == 4) && (rolls[4] == 5)) ||
			((rolls[1] == 3) && (rolls[2] == 4) && (rolls[3] == 5) && (rolls[4] == 6)))
		{
			Score[9][1] = 30;

		}
		Score[9][0] = 1;
		std::cout << "\n";
		std::cout << "\n\n\tTotal value of your *Small Straight*: \n";
		std::cout << "\t" << Score[9][1] << std::endl;
	}
	else
	{
		std::cout << "\n\n\tYou've already filled this category! Please choose another\n";
		this->displayChoices(rolls);
	}

}
void Scoreboard::Chance(int rolls[]) {

	if (Score[12][0] == false) {
		for (int i = 0; i < 5; i++) {
			Score[12][1] += rolls[i];
		}
		Score[12][0] = 1;

	}
	else
	{
		std::cout << "\n\n\tYou've already filled this category! Please choose another\n";
		this->displayChoices(rolls);
	}


}
void Scoreboard::FullHouse(int rolls[]) {

	if (Score[8][0] == false) {
		if ((((rolls[0] == rolls[1]) && (rolls[1] == rolls[2])) && // Three of a Kind
			(rolls[3] == rolls[4]) && // Two of a Kind
			(rolls[2] != rolls[3])) ||
			((rolls[0] == rolls[1]) && // Two of a Kind
			((rolls[2] == rolls[3]) && (rolls[3] == rolls[4])) && // Three of a Kind
				(rolls[1] != rolls[2])))
		{
			Score[8][1] = 25;
		}
		Score[8][0] = 1;
		std::cout << "\n";
		std::cout << "\n\n\tTotal value of your *Full House*: \n";
		std::cout << "\t" << Score[8][1] << std::endl;

	}
	else
	{
		std::cout << "\n\n\tYou've already filled this category! Please choose another\n";
		this->displayChoices(rolls);
	}

}

int Scoreboard::sumScores() {

	int totalScore = 0;
	for (int i = 0; i < 13; i++) {

		totalScore += Score[i][1];
	}
	return totalScore;
}

bool Scoreboard::displayFinalScores() {

	int UpperBoard = 0;
	int totalScore = 0;
	for (int i = 0; i < 6; i++) {

		UpperBoard += Score[i][1];

	}
	if (UpperBoard > 63) {
		totalScore = this->sumScores() + 35;
	}
	else
	{
		totalScore = this->sumScores();
	}

	system("cls");

	std::cout << "\tCongratulations! Your final score is: " + std::to_string(totalScore) + "!\n\t";

	return true;
}

bool Scoreboard::is_number(const std::string& s)
{
	std::string::const_iterator it = s.begin();
	while (it != s.end() && isdigit(*it)) ++it;
	return !s.empty() && it == s.end();
}

// Getters for testing
int Scoreboard::getAces()
{
	return Score[0][1];
}

int Scoreboard::getTwos()
{
	return Score[1][1];
}

int Scoreboard::getThrees()
{
	return Score[2][1];
}

int Scoreboard::getFours()
{
	return Score[3][1];
}

int Scoreboard::getFives()
{
	return Score[4][1];
}


int Scoreboard::getSixes()
{
	return Score[5][1];
}

int Scoreboard::getThreeOfAKind()
{
	return Score[6][1];
}


int Scoreboard::getFourOfAKind()
{
	return Score[7][1];

}

int Scoreboard::getYahtzee()
{
	return Score[11][1];
}


int Scoreboard::getLargeStraight()
{
	return Score[10][1];
}
int Scoreboard::getSmallStraight()
{
	return Score[9][1];

}


int Scoreboard::getChance()
{
	return Score[12][1];

}


int Scoreboard::getFullHouse()
{
	return Score[8][1];

}
