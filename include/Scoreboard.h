#pragma once
#include <string>
#include <iostream>
class Scoreboard
{

private:
	//Array to keep tack of score and whether or not the score has been locked in
	//Score[Score category][PointValue//locked]
	//Score[0][0] == bool value if locked in.
	//Score[0][1] == score value locked in.
	int Score[13][2];
	bool is_number(const std::string& s);
	int sumScores();
	void displayUsed(int rolls[]);

public:
	// void displayChoices(int rolls[]);
	// void grabChoice(int rolls[]);
	// void displayFinalScores();
	// int Score[13][2];	
	// bool is_number(const std::string& s);
	// int sumScores();
	// // void displayUsed(int* rollArray);
	// std::string convertArray(int* rollArray);
	bool isDisplayChoicesCalled = false;
	void Aces(int* rollArray);
	void Twos(int* rollArray);
	void Threes(int* rollArray);
	void Fours(int* rollArray);
	void Fives(int* rollArray);
	void Sixes(int* rollArray);
	void ThreeOfAKind(int* rollArray);
	void FourOfAKind(int* rollArray);
	void Yahtzee(int* rollArray);
	void LargeStraight(int* rollArray);
	void SmallStraight(int* rollArray);
	void Chance(int* rollArray);
	void FullHouse(int* rollArray);
	//getters everywhere
	int getAces();
	int getTwos();
	int getThrees();
	int getFours();
	int getFives();
	int getSixes();
	int getThreeOfAKind();
	int getFourOfAKind();
	int getYahtzee();
	int getLargeStraight();
	int getSmallStraight();
	int getChance();
	int getFullHouse();

	Scoreboard();

	~Scoreboard();

	bool displayChoices(int* rollArray);
	void grabChoice(int* rollArray, std::string Choice);
	bool displayFinalScores();
};
