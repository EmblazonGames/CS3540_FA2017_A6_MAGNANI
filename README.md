# Project 6: Test Driven Development

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- GNU Make
- G++

## Installing the Project

In order to install the project, pull from the provided Git link, either
via SSH or HTML, into a directory on your local computer.

Alternatively, you may download the .zip version and unzip it to a directory 
on your local computer.

Open a command line.

Navigate to the directory you put the Gitlab project into, then navigate to the 
subdirectory build.

Then, see directions for running the tests.

## Running the Yahtzee program

To run Yahtzee once in the project directory in the command line,
run the command:

make all

Afterwards, you can navigate up a directory using .. , then,
you can navigate to the newly created bin directory.

Then you can run Yahtzee.exe and play the game!

## Running the tests

To run tests once in the project directory in the command line,
run the command:

make test

Afterwards, you can navigate up a directory using .. , then,
you can navigate to the newly created bin directory.

Then you can run Yahtzee_die_Tests.exe and it will test the
code.

## Deployment

This project will automatically use the .gitlab-ci.yml to integrate with 
GitLab's Continuous Integration System. No further deployment is necessary.

## Built With

* Visual Studio
* G++
* Make
* GitKraken
* GitLab
* Notepad++

## Authors
 
- Susan Magnani
- Morgan Mastrocinque
- Liam Gill
- Wesley Byers

## License

This project is licensed under All Rights Reserved (C) 2017 BGSU.

## Acknowledgments

* Dr. Green :)
