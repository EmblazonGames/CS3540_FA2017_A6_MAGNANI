#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#define CATCH_CONFIG_FAST_COMPILE
#include "catch.hpp"
#include <iostream>

#include "../include/Scoreboard.h"
#include "../include/Dice.h"
#include "../include/Game.h"
#include "../include/Scoreboard.h"
#include "../include/Scoreboard.h"


SCENARIO( "A yahtzee Die object can be created", "[die]" ) {

    GIVEN( "A Yahtzee die object" ) {
       Die* D1 = nullptr;
        
        WHEN( "You try to instantiate the die object" ) {
			
			D1 = new Die;
			
            THEN( "the die was instantiated" ) {
                REQUIRE( D1 != nullptr);
            }
        }

        WHEN( "You try to instantiate the die object and roll the die" ) {
            
            D1 = new Die;
            int val = D1->roll();
            
            THEN( "the die will return a number between 1-6" ) {
                REQUIRE( (val >= 1 && val <=6));
            }
            
        }
    }
    
        GIVEN( "A Yahtzee dice object" ) {
       Dice* Dice1 = nullptr;
        
        WHEN( "You try to instantiate the dice object" ) {
            
            Dice1 = new Dice;
            
            THEN( "the die was instantiated" ) {
                REQUIRE( Dice1 != nullptr);
            }
        }
         WHEN( "You try to instantiate the dice object and roll" ) {
            
            Dice1 = new Dice;
            int* resultReturned = Dice1->RollDice();
            
            THEN( "the die were rolled, then you should get a int array" ) {
                REQUIRE((resultReturned[0] >= 1));
                REQUIRE((resultReturned[1] >= 1));
                REQUIRE((resultReturned[2] >= 1));
                REQUIRE((resultReturned[3] >= 1));
                REQUIRE((resultReturned[4] >= 1));
            }
        }
        WHEN( "You try to instantiate the dice object and roll" ) {
            
            Dice1 = new Dice;
            int* resultReturned = Dice1->RollDice();
            
            THEN( "the die were rolled, then you should get a int array in sorted order" ) {
                REQUIRE((resultReturned[0] <= resultReturned[1]));
                REQUIRE((resultReturned[1] <= resultReturned[2]));
                REQUIRE((resultReturned[2] <= resultReturned[3]));
                REQUIRE((resultReturned[3] <= resultReturned[4]));

            }
        }

}

   GIVEN( "A Yahtzee game object" ) {
      Game* G1 = nullptr;
        
        WHEN( "You try to start the game" ) {
			
			G1 = new Game;
			G1->Play();
            THEN( "the dice & Scoreboard were created" ) {
                REQUIRE(G1->D1 != nullptr);
				REQUIRE(G1->S1 != nullptr);
            }
        }
		WHEN( "You try to start the game & roll" ) {
			
			G1 = new Game;
			G1->Play();
            THEN( "Scoreboard displays choices" ) {
				REQUIRE(G1->S1->displayChoices(G1->D1->RollDice()) == true);
            }
        }
		WHEN( "You try to start the game & roll & pick a choice & repeat until the game is over" ) {
			
			G1 = new Game;
			G1->Play();
			for(int i=1; i <= 13; i++){
				int* temp = G1->D1->RollDice();
				G1->S1->isDisplayChoicesCalled = G1->S1->displayChoices(temp);
				G1->S1->grabChoice(temp, std::to_string(i));
			}
            THEN( "Scoreboard displays final score" ) {
				REQUIRE(G1->S1->displayFinalScores() == true);
            }
        }

    }

    GIVEN( "A Yahtzee scoreboard object" ) {
    
        Scoreboard* S1 = nullptr;
        Dice D1;
    
        WHEN( "You try to instantiate the scoreboard object" ) {
        
            S1 = new Scoreboard();
        
            THEN( "the scoreboard was instantiated" ) {
                REQUIRE( S1 != nullptr);
            }
        }

        WHEN( "You try to call getAces" ) {
            
                S1 = new Scoreboard();
            
                THEN( "you should get zero" ) {
                    REQUIRE( S1->getAces() == 0);
                }
        }

        WHEN( "You try to call Aces with some ones in the roll" ) {
            
                S1 = new Scoreboard();
                int result[] = {1,1,1,1,1};
                S1->Aces(result);

                THEN( "you should get non zero" ) {
                    REQUIRE( S1->getAces() != 0);
                }
        }

        WHEN( "You try to call getTwos" ) {
            
                S1 = new Scoreboard();
            
                THEN( "you should get zero" ) {
                    REQUIRE( S1->getTwos() == 0);
                }
        }

        WHEN( "You try to call getThrees" ) {
            
                S1 = new Scoreboard();
            
                THEN( "you should get zero" ) {
                    REQUIRE( S1->getThrees() == 0);
                }
        }

        WHEN( "You try to call getFours" ) {
            
                S1 = new Scoreboard();
            
                THEN( "you should get zero" ) {
                    REQUIRE( S1->getFours() == 0);
                }
        }

        WHEN( "You try to call getFives" ) {
            
                S1 = new Scoreboard();
            
                THEN( "you should get zero" ) {
                    REQUIRE( S1->getFives() == 0);
                }
        }

        WHEN( "You try to call getSixes" ) {
            
                S1 = new Scoreboard();
            
                THEN( "you should get zero" ) {
                    REQUIRE( S1->getSixes() == 0);
                }
        }

        WHEN( "You try to call Twos with some twos in the roll" ) {
            
                S1 = new Scoreboard();
                int result[] = {2,2,2,2,2};
                S1->Twos(result);

                THEN( "you should get non zero" ) {
                    REQUIRE( S1->getTwos() != 0);
                }
        }

        WHEN( "You try to call Threes with some threes in the roll" ) {
            
                S1 = new Scoreboard();
                int result[] = {3,3,3,3,3};
                S1->Threes(result);

                THEN( "you should get non zero" ) {
                    REQUIRE( S1->getThrees() != 0);
                }
        }

        WHEN( "You try to call Fours with some fours in the roll" ) {
            
                S1 = new Scoreboard();
                int result[] = {4,4,4,4,4};
                S1->Fours(result);

                THEN( "you should get non zero" ) {
                    REQUIRE( S1->getFours() != 0);
                }
        }

        WHEN( "You try to call Fives with some fives in the roll" ) {
            
                S1 = new Scoreboard();
                int result[] = {5,5,5,5,5};
                S1->Fives(result);

                THEN( "you should get non zero" ) {
                    REQUIRE( S1->getFives() != 0);
                }
        }

        WHEN( "You try to call Sixes with some sixes in the roll" ) {
            
                S1 = new Scoreboard();
                int result[] = {6,6,6,6,6};
                S1->Sixes(result);

                THEN( "you should get non zero" ) {
                    REQUIRE( S1->getSixes() != 0);
                }
        }
    }
		GIVEN("A Scoreboard and Dice Class are created.") {
			Scoreboard S1;
			Dice D1;

			WHEN("The Player attempts to Play 3 of a kind, or 4 of a kind") {

				int test[] = { 2, 2, 2, 2, 6, };


				S1.ThreeOfAKind(test);
				S1.FourOfAKind(test);


				THEN("Test 3 of a king, 4 of a kind, yahtzee, and chance") {
					REQUIRE(S1.getThreeOfAKind() == 14);
					REQUIRE(S1.getFourOfAKind() == 14);

				}
			}
			WHEN("The Player attempts to Play Yahtzee or Chance") {

				int test2[] = { 1, 1, 1, 1, 1, };

				S1.Chance(test2);
				S1.Yahtzee(test2);
				

				THEN("Test Yahtzee and Chance") {

					REQUIRE(S1.getChance() == 5);
					REQUIRE(S1.getYahtzee() == 50);

				}
			


			}

			WHEN("The Player attempts to Play Small or Large Straight"){

				int test3[] = { 1, 2, 3, 4, 5, };

			
				S1.LargeStraight(test3);
				S1.SmallStraight(test3);

				THEN("Test Small and Large Straight") {
				
				REQUIRE(S1.getSmallStraight() == 30);
				REQUIRE(S1.getLargeStraight() == 40);
			}

		}

			WHEN("The Player attempts to Play Full house") {
				int test4[] = { 2, 2, 2, 3, 3, };

				S1.FullHouse(test4);
				THEN("Test full House") {

					REQUIRE(S1.getFullHouse() == 25);
				}
			}
		}
}
